# Set-ExecutionPolicy RemoteSigned
# Set-ExecutionPolicy Restricted

$baseSheetIDs = @{}
$timestampSheetIDs = @{}

$baseSheetIDs.Add("shiragap", "1zrlP273XR5J9Hh7SHuNs8kuq0PwS8PUMbOES1fo9PXw")
$timestampSheetIDs.Add("shiragap", "1puaUMwPM06rWRiGNmKJQei2c0GwzumBWFC68KW6tkck")

$baseSheetIDs.Add("urig01", "1zrlP273XR5J9Hh7SHuNs8kuq0PwS8PUMbOES1fo9PXw")
$timestampSheetIDs.Add("urig01", "1puaUMwPM06rWRiGNmKJQei2c0GwzumBWFC68KW6tkck")

$baseSheetIDs.Add("urig02", "1__fozWR10lnNLzYQmwvmo4fjgcod57Dmkz_1PdF7Ddc")
$timestampSheetIDs.Add("urig02", "1BYwjmEk0PmBZ6zB7oK4qKpdjYyeP8cPSSTKRhGtcOqg")

$baseSheetIDs.Add("urig03", "1ICdABDjRVuQQZnFn1dNlRBYTT9wSb9o4d7h4Mzpai4s")
$timestampSheetIDs.Add("urig03", "10dFr3vPvisfqS977LHGGYmyVKdwGdoRgo6BfPnx7-es")

$timestampSheetIDs.Add("urig04", "1FN3RrYjldaDBqGGdCCVlmAJQtc0yDUkP1syBFu39QjA")

$baseSheetIDs.Add("urig05", "1cZNUvJ5aQpPR9zqIirOQsWgFBXIYfJwmKWJDhpP67J8")
$timestampSheetIDs.Add("urig05", "13AG8zHL1yyKqEO5B5cdSjPfJ5sYNahglARgA7JD3QVw")

$baseSheetIDs.Add("urig06", "1FwcxUOZVbgQ8dvnAMAD4LWCBshs4SQUd5UVpHCDwmWM")
$timestampSheetIDs.Add("urig06", "1fa6hArt1Xl6l774F75ix1hOWKOvDSqoCrBMU9iH_fYI")

$baseSheetIDs.Add("urig07", "156O7Up_Q7jZ1063QXV_wN3-poVP5hUcOHkWBgRQ-z64")
$timestampSheetIDs.Add("urig07", "1hBUF2jWjvJEgC8eBDRdJLjlZE6pmcXGLSOs9NV2Cudo")

$baseSheetIDs.Add("urig08", "1-U1z-2l8PMVuIpYRlbQz7g_NQAWaJ3ZJcwa0VYLLCK0")
$timestampSheetIDs.Add("urig08", "1YjagOCzxpzz88QFMFTwjIV_cjYH_MKF6pz7ap77PPsU")

$baseSheetIDs.Add("urig09", "1TTc5K_aMJ1DZ0JPTr9rXvhjjgGR2a6FueX3mD5HpRwM")
$timestampSheetIDs.Add("urig09", "1OqLDPg9tCV1pWTtWz_RgnFOlX6lBaiWIMnYJhK-DQG4")

$baseSheetIDs.Add("urig10", "1tt1SB5tLl8B6CPC602gr5IC1LO2VEw0al2SbWnfu3Tk")
$timestampSheetIDs.Add("urig10", "17VPOG3FTKMlptbb1NEI6osx18cNW_lKA1-NIA1zT6xc")

$baseSheetIDs.Add("urig11", "1FflNHjoD7pb9znveljWjN0NhPOCYyAXGFRdu_7ZFhqs")
$timestampSheetIDs.Add("urig11", "1wUsa_Vp-ZK7QUq0fj8IHgQmyIpAsym6XlSah-NHRoDI")

$baseSheetIDs.Add("urig12", "15s2yTv08WHN1GQpEwVjA6XVpmR0Ty4fl50KBO2vCews")
$timestampSheetIDs.Add("urig12", "1AY1i9t5R7XUZ2Vn1VfKyUQi4-27qOT5mZnjHhafk9Ag")

$baseSheetIDs.Add("urig13", "1PGdcIzD-bn8OhzVXBueLyIBUP8lY5Wwb6uBxJISLCF4")
$timestampSheetIDs.Add("urig13", "1b6Eyu5XX_K8uIC_zd5v1Gbf_bcZ6jMEYpcm02Ctdh2g")

$baseSheetIDs.Add("urig14", "1-wch6vdI4im9R3m0MY9ykzXs_F5RzujZd-_UIIP5fbg")
$timestampSheetIDs.Add("urig14", "1Dt9F7UTLvfwNNXq-HqAHdoKHs9xTLAf8ilD4p_TZbfA")

$baseSheetIDs.Add("urig15", "1sQT3f0gFr1Ia9X5bqT7IInidAAQG9bkuB2HYfp4G6NI")
$timestampSheetIDs.Add("urig15", "15b5F1OigCwu-pUBhxRGgSWJPDa583YLAHPJ4fcn_DuU")

$baseSheetIDs.Add("urig16", "1rHQrNOjh8FhZ2ct5wRq3wfrI_UFVDaGxdhui4ERtzn4")
$timestampSheetIDs.Add("urig16", "1sFnoBFmP6meALfR-pB1dhoMC5apfJtizyd3JDCqKWEU")

$baseSheetIDs.Add("urig17", "1oTxUFCW4d1fLdlxdMLjj2p1qIKhdDLoCNX-FiTp4REY")
$timestampSheetIDs.Add("urig17", "1gZB6CEUxIslgEBna4dMGil8wCQJuJyNs6cwl-6LOAJY")

$baseSheetIDs.Add("urig18", "1lm3HegIC406SN9PFRjzpzRlQO2RNGOVjC08PxBdg8Fk")
$timestampSheetIDs.Add("urig18", "1b5mVnZ3R4TTVAydnIR3mKWznS6VLjYn_hr1cnBkrPk4")

$baseSheetIDs.Add("urig19", "1OTZw88gfEgul7BNFqUKP72SuPeQ2XHE-KZiYbNoFR_U")
$timestampSheetIDs.Add("urig19", "1Q3sLJItQMprFjWoiN_hNb79y19xBYN6QZcTd3wUvJ2o")

$baseSheetIDs.Add("urig20", "1G5K91vYJK48P2r2hG0db-s3xpmAzao9nxk9kuvJR_QU")
$timestampSheetIDs.Add("urig20", "1K7OadBpUQYSfMaEgKfD0J-iR5wvdr2gRwpyHMZrT5RA")

$baseSheetIDs.Add("urig21", "1PvvvK9v-tug9qKcfAkw2yU8msoGwjRJqOR6bPSLwbns")
$timestampSheetIDs.Add("urig21", "1JY98ZzcR_RID_PY8WmKGL0UTQczSXONoxYyHoDS0LEQ")

$baseSheetIDs.Add("urig22", "1AL5YjPCJ9qEYCd27OIczNxKo0kg2vsl_mi5M1ldKg3M")
$timestampSheetIDs.Add("urig22", "1yCzniOh6uHmu5wQ2UXOIcUYR8ToVTeIsWJwTsI9M-Qo")

$baseSheetIDs.Add("urig23", "1s9aVbXIHbXp4LOVGsNlKTM9QkcJbs_oQEqP41caKKNU")
$timestampSheetIDs.Add("urig23", "10U7F_3cG2OLiv0BLIzV_LJ3Tu5NwjqMip8qpIMG_W8c")

$baseSheetIDs.Add("urig24", "1E9A1FcYIh0n5dmtQKL1-WSX6vCb2nlrxm3VhNoYyRwE")
$timestampSheetIDs.Add("urig24", "1tNp90wXLvAFinuBdgG0gIu2j8K4cBRfcl3dsJCUVixE")

$baseSheetIDs.Add("urig25", "1sUROVpEtErxzBf6lCnG8OnnoesOi6vLG_pyB6GNO1u0")
$timestampSheetIDs.Add("urig25", "1TRO5wKikBpiDGYzsedHMwrHbzZiZOQu_Bp-pMA9rIjw")

If ($IsWindows) {
	$username = $env:USERNAME.toLower()
}
Elseif ($IsLinux) {
	$username = Invoke-Command -ScriptBlock {whoami}
}
$baseSheetUri = "https://docs.google.com/spreadsheets/d/" + $baseSheetIDs[$username]
$timestampSheetUri = "https://docs.google.com/spreadsheets/d/" + $timestampSheetIDs[$username]
$monitorSheetUri = "https://script.google.com/macros/s/AKfycbx1o06YKjf-WpVD1buEOLVMHNz0tKgaIY6mNMMxVo4uNPl2kK6B/exec"

$minerNames = @("t-rex", "z-enemy", "PhoenixMiner", "noncerpro", "nbminer", "miner", "CryptoDredge", "lolMiner", "danila-miner")

If ($IsLinux) {
	Start-Process chmod -ArgumentList "775 ./linux/noncerpro-cuda-linux-3.3.1/noncerpro"
	Start-Process chmod -ArgumentList "775 ./linux/z-enemy-2.6.2-cuda101-libcurl4/z-enemy"
	Start-Process chmod -ArgumentList "775 ./linux/t-rex-0.19.11-linux-cuda10.0/t-rex"
	Start-Process chmod -ArgumentList "775 ./linux/PhoenixMiner_5.9d_Linux/PhoenixMiner"
	Start-Process chmod -ArgumentList "775 ./linux/gminer_2_39_linux64/miner"
	Start-Process chmod -ArgumentList "775 ./linux/lolMiner_v1.44_Lin64/lolMiner"
}

function Download-Start () {
	Do {
		echo "Download start.bat"
		Invoke-WebRequest -Uri "$($baseSheetUri)/export?format=tsv&gid=0" -OutFile "start.bat"
		$fileContent = Get-Content "start.bat"
		$containFlag = $fileContent | %{$_  -Match "#N/A"}
		If ($containFlag -ne 0) {
			sleep 2
		}
	} While ($containFlag -ne 0)
}

function Start-Process-OS ($param) {
	If ($IsWindows) {
		return Start-Process -FilePath $param
	}
	Elseif ($IsLinux) {
		return Start-Process gnome-terminal -ArgumentList "-- sh ./$($param)"
	}
}

function Stamp-Time () {
	If ($IsWindows) {
		$spreadsheetTimestamp = Start-Process -FilePath msedge -arg "--headless --disable-gpu --remote-debugging-port=33888 $($timestampSheetUri)" -PassThru
		sleep 30
		
		$temperature = Invoke-Command -ScriptBlock {.\nvidia-smi --format=csv,noheader --query-gpu=temperature.gpu}
		$temperature = $temperature -join ","
		$monitorUri = "$($monitorSheetUri)?rig=$($username)&temperature=$($temperature)"
		$spreadsheetMonitor = Start-Process -FilePath msedge -arg "--headless $($monitorUri)" -PassThru
		sleep 15
		$spreadsheetTimestamp.Kill()
		$spreadsheetMonitor.Kill()
	}
	Elseif ($IsLinux) {
		$spreadsheetTimestamp = Start-Process firefox -arg "--headless --private-window $($timestampSheetUri)" -PassThru
		sleep 30
		
		$temperature = Invoke-Command -ScriptBlock {nvidia-smi --format=csv,noheader --query-gpu=temperature.gpu}
		$temperature = $temperature -join ","
		$monitorUri = "$($monitorSheetUri)?rig=$($username)&temperature=$($temperature)"
		
		Stop-Process -Name "firefox" -Force
		Stop-Process -Name "MainThread" -Force
		sleep 2
		
		$spreadsheetMonitor = Start-Process firefox -arg "--headless --private-window $($monitorUri)" -PassThru
		sleep 15
		Stop-Process -Name "firefox" -Force
		Stop-Process -Name "MainThread" -Force
	}
}

function Reset-GPUs () {
	If ($IsWindows) {
		echo "Reset-GPUs!!"
		$gpus = Get-PnpDevice -FriendlyName 'NVIDIA GeForce *'
		Disable-PnpDevice $gpus.InstanceId -Confirm:$False
		sleep 2
		Enable-PnpDevice $gpus.InstanceId -Confirm:$False
		sleep 3
	}
}

Download-Start
Invoke-WebRequest -Uri "$($baseSheetUri)/export?format=tsv&gid=1885128687" -OutFile "stop.bat"

Start-Process-OS "start.bat"

echo "Start!"

while ($true)
{
	Stamp-Time
				
	For ($updateCounter=0; $updateCounter -lt 2; $updateCounter++) {
		# 10x30 = 300 seconds for next bat download
		For ($sleepCounter=0; $sleepCounter -lt 10; $sleepCounter++) {
			$runningMinersNumber = 0
			For ($idx=0; $idx -lt $minerNames.Length; $idx++) {
				$minerProcess = Get-Process $minerNames[$idx] -ErrorAction SilentlyContinue
				If ($minerProcess) {
                    If ($minerNames[$idx] -eq "t-rex") {
					    $runningMinersNumber += ($minerProcess.Length / 2)
                    }
                    Elseif ($minerNames[$idx] -eq "nbminer") {
					    $runningMinersNumber += ($minerProcess.Length / 2)
                    }
                    Elseif ($minerNames[$idx] -eq "miner") {
					    $runningMinersNumber += ($minerProcess.Length / 2)
                    }
                    Elseif ($minerNames[$idx] -eq "danila-miner") {
					    $runningMinersNumber += ($minerProcess.Length / 2)
                    }
                    Else {
					    $runningMinersNumber += $minerProcess.Length
                    }
                    $minerProcessLength = $minerProcess.Length -as [int]
                    $minerProcessName = $minerNames[$idx]
                    echo "$minerProcessLength $minerProcessName"
                    echo "runningMinersNumber $runningMinersNumber"
				}
			}
			If ($runningMinersNumber -ne 1) {
				echo "More than 1 miner or no miner is running"
				echo "Rerun!!"
				Add-Content "start.bat" "`n::Rerun"
				git clean -f -e "run.bat" -e "run.ps1" -e "start.bat"
				git pull
				sleep 1
				Break
			}
			Else {
				If ($sleepCounter -eq 5) {
					Stamp-Time
				}
				Else {
					sleep 20
				}
			}			
		}
		
		Copy-Item "start.bat" "start.bat.tmp"
		Download-Start

		If(Compare-Object -ReferenceObject $(Get-Content "start.bat") -DifferenceObject $(Get-Content "start.bat.tmp")) {
			echo "Switch!"
			Start-Process-OS "stop.bat"
			For ($idx=0; $idx -lt $minerNames.Length; $idx++) {
				Stop-Process -Name $minerNames[$idx] -ErrorAction SilentlyContinue
			}
			Invoke-WebRequest -Uri "$($baseSheetUri)/export?format=tsv&gid=1885128687" -OutFile "stop.bat"
			sleep 2
			#Reset-GPUs
			#sleep 2
			Start-Process-OS "start.bat"
			sleep 10
		}
		Else {
			echo "Stay..."
		}
	}
}
