@echo off

timeout 3

cd /d %~dp0

git clean -f -e "run.bat" -e "run.ps1"
git pull

set Command="& .\run.ps1"

where pwsh >nul 2>nul || goto powershell
pwsh -executionpolicy bypass -command %Command%
goto end

powershell -version 5.0 -executionpolicy bypass -command %Command%
