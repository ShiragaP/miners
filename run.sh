sleep 3

cd "$(dirname "$0")"

git clean -f -e "run.bat" -e "run.ps1"
git reset --hard
git pull

pwsh run.ps1 -version 5.0 -executionpolicy bypass
